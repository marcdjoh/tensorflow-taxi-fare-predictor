# Create a composer environment named 'data-synchronization-env'
gcloud composer environments create ml-model-retraining-prod --location=europe-west1 --zone=europe-west1-b

# Create the table 'training_jobs'
CREATE TABLE training_jobs (
    jobName varchar(200) NOT NULL,
    versionName varchar(200) NOT NULL,
    evalLoss numeric NOT NULL,
    labelMean numeric NOT NULL);

# Create the table 'predictions'
CREATE TABLE predictions (
versionName varchar(200) NOT NULL,
pickuplon numeric NOT NULL,
pickuplat numeric NOT NULL,
dropofflat numeric NOT NULL,
dropofflon numeric NOT NULL,
passengers integer NOT NULL,
prediction numeric NOT NULL);

# Clone the gitlab repository
git clone https://gitlab.com/marcdjoh/tensorflow-taxi-fare-predictor.git

# Navigate to the manifest folder
cd manifests

# Create the namespace
kubectl apply -f namespace.yaml

# Create the deployment
kubectl apply -f pod.yaml

# Create the service
kubectl apply -f service.yaml

# Check if there is version available for the taxi_fare_predictor model
gcloud ai-platform versions list --model=taxi_fare_predictor