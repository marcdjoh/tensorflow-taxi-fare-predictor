import os
import random

from airflow import DAG
from datetime import datetime

from airflow.operators.python_operator import PythonOperator
from airflow.hooks.postgres_hook import PostgresHook

from googleapiclient import discovery, errors

import utils

MODEL_NAME = 'taxi_fare_predictor'

# args
DEFAULT_ARGS = {
    'owner': 'Marc Djohossou',
    'depend_on_past': False,
    'start_date': datetime(2018, 1, 1, 10, 00, 00),
    'email': []
}

dag_id = 'prediction-requestor-dag'
dag = DAG(dag_id,
          default_args=DEFAULT_ARGS,
          schedule_interval='*/5 * * * *',
          catchup=False)

drifted_dag_id = 'drifted-prediction-requestor-dag'
drifted_dag = DAG(drifted_dag_id,
                  default_args=DEFAULT_ARGS,
                  schedule_interval='*/5 * * * *',
                  catchup=False)


def predict_json(model_name, instances):
    """Send json data to a deployed model for prediction.
    """
    # Create the ML Engine service object.
    # To authenticate set the environment variable
    # GOOGLE_APPLICATION_CREDENTIALS=<path_to_service_account_file>
    service = discovery.build('ml', 'v1')
    name = 'projects/{}/models/{}'.format(os.environ['GCP_PROJECT'], model_name)

    response = service.projects().predict(
        name=name,
        body={'instances': instances}
    ).execute()
    if 'error' in response:
        raise RuntimeError(response['error'])

    return response['predictions']


def sample_model_inputs(nb=2):
    instances = []
    for i in range(nb):
        instance = {'pickuplon': random.uniform(-71, -75),
                    'pickuplat': random.uniform(38, 42),
                    'dropofflon': random.uniform(-71, -75),
                    'dropofflat': random.uniform(38, 42),
                    'passengers': random.randint(1, 10)}
        instances.append(instance)
    return instances


def sample_model_drifted_inputs(nb=2):
    instances = []
    for i in range(nb):
        instance = {'pickuplon': random.uniform(0, 1),
                    'pickuplat': random.uniform(1, 2),
                    'dropofflon': random.uniform(3, 4),
                    'dropofflat': random.uniform(4, -5),
                    'passengers': random.randint(1000, 2000)}
        instances.append(instance)
    return instances


def predict_and_store(model_name, _predictor):
    version_name = utils.get_model_current_version(model_name)
    instances = _predictor()
    predictions = predict_json(model_name, instances)
    rows = []
    for idx, pred in enumerate(predictions):
        input = instances[idx]
        prediction = pred['predictions'][0]
        rows.append((version_name, input['pickuplon'], input['pickuplat'], 
                     input['dropofflat'], input['dropofflon'], input['passengers'], prediction))
    
    hook = PostgresHook('cloud_sql_proxy_conn')
    hook.insert_rows('predictions', rows)


requestor_task = PythonOperator(
    task_id='requestor',
    python_callable=predict_and_store,
    op_kwargs={'model_name': MODEL_NAME, '_predictor': sample_model_inputs},
    dag=dag
)

drifted_requestor_task = PythonOperator(
    task_id='requestor',
    python_callable=predict_and_store,
    op_kwargs={'model_name': MODEL_NAME, '_predictor': sample_model_drifted_inputs},
    dag=drifted_dag
)
    