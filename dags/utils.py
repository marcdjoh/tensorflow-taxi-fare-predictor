from googleapiclient import discovery, errors
import os

def get_model_current_version(model_name):
    ml = discovery.build('ml', 'v1')
    project_id = 'projects/{}'.format(os.environ['GCP_PROJECT'])
    model_id = '{}/models/{}'.format(project_id, model_name)
    # Create a request to call projects.models.versions.list.
    request = ml.projects().models().versions().list(parent=model_id)
    try:
        response = request.execute()
        for version in response.get('versions', []):
            if version.get('isDefault'):
                return version['name'].split('/')[-1]
        return None
    except errors.HttpError as err:
        # Something went wrong, print out some information.
        print('There was an error listing the versions. Check the details:')
        print(err._get_reason())