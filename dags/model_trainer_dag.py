import os
import numpy as np
from datetime import datetime

from airflow import DAG


from airflow.operators.python_operator import BranchPythonOperator
from airflow.operators.bash_operator import BashOperator
from airflow.contrib.operators.mlengine_operator import MLEngineTrainingOperator, \
    MLEngineVersionOperator
from airflow.providers.google.cloud.operators.mlengine import MLEngineSetDefaultVersionOperator
from airflow.hooks.postgres_hook import PostgresHook

from google.cloud import storage

import utils

MODEL_NAME = 'taxi_fare_predictor'
CLOUD_SQL_INSTANCE_NAME = 'ml-model-retraining-dev'
CLOUD_SQL_DB_NAME = 'retraining-db'
CLOUD_SQL_VERSIONS_TABLE_NAME = 'versions'
CLOUD_SQL_PREDICTIONS_TABLE_NAME = 'predictions'

# args
DEFAULT_ARGS = {
    'owner': 'Marc Djohossou',
    'depend_on_past': False,
    'start_date': datetime(2018, 1, 1, 10, 00, 00),
    'email': []
}

dag_id = 'model-trainer-dag'
dag = DAG(dag_id,
          default_args=DEFAULT_ARGS,
          schedule_interval=None,
          catchup=False)


def check_job_relevancy(_ts, **kwargs):
    folder = 'model/{}/export/exporter/'.format(_ts)
    storage_client = storage.Client()
    bucket = storage_client.get_bucket(os.environ['GCP_PROJECT'])
    blobs = bucket.list_blobs(prefix=folder)
    blob_name = None
    for blob in blobs:
        tmp = len(blob.name.rstrip('/').split('/'))
        if tmp == 5:
            blob_name = blob.name
    kwargs['ti'].xcom_push(key='model-location', value='gs://{}/{}'.format(os.environ['GCP_PROJECT'], blob_name))
    current_version = utils.get_model_current_version(MODEL_NAME)

    if current_version is None:
        return ['version-creation', 'default-version-setting']
    
    candidate_version_metric = get_version_metric('version_{}'.format(_ts))
    current_version_metric = get_version_metric(current_version)
    if current_version_metric > candidate_version_metric:
        return ['version-creation']
    else:
        return ['stop']


def get_version_metric(version_name):
    sql_string = "SELECT evalLoss FROM training_jobs WHERE versionName = '" + version_name + "'"
    hook = PostgresHook('cloud_sql_proxy_conn')
    res = hook.get_records(sql_string)
    try:
        eval_loss = float(res[0][0])
    except IndexError:
        eval_loss = np.inf
    return eval_loss


def download_blob(source_bucket_name, source_blob_name, dest_file_path):
    storage_client = storage.Client()

    bucket = storage_client.bucket(source_bucket_name)
    blob = bucket.blob(source_blob_name)
    blob.download_to_filename(dest_file_path)


def store_training_job_metrics(context):
    download_blob(os.environ['GCP_PROJECT'], 'tmp/metrics.csv', '/tmp/metrics.csv')
    with open('/tmp/metrics.csv', 'r') as f:
        lines = f.readlines()
    _, eval_loss, label_mean = lines[-1].split(',')
    hook = PostgresHook('cloud_sql_proxy_conn')
    _format = '%Y%m%dT%H%M%S'
    execution_date = context.get('execution_date').strftime(_format)
    training_jobs = hook.insert_rows('training_jobs', 
            [('{}_{}'.format(MODEL_NAME, execution_date), 
              'version_{}'.format(execution_date), 
              float(eval_loss), 
              float(label_mean))])


training_task = MLEngineTrainingOperator(
    task_id="training",
    project_id=os.environ['GCP_PROJECT'],
    region='europe-west1',
    job_id='taxi_fare_predictor_{{ ts_nodash }}',
    runtime_version='1.14',
    python_version='3.5',
    job_dir='gs://{}/model/'.format(os.environ['GCP_PROJECT']) + '{{ ts_nodash }}' + '/',
    package_uris=['gs://{}/source/taxifare-0.1.tar.gz'.format(os.environ['GCP_PROJECT'])],
    training_python_module='trainer.task',
    training_args=['--train_data_paths=gs://{}/data/taxi-train.csv'.format(os.environ['GCP_PROJECT']),
                   '--eval_data_paths=gs://{}/data/taxi-valid.csv'.format(os.environ['GCP_PROJECT']),
                   '--output_dir=gs://{}/model/'.format(os.environ["GCP_PROJECT"]) + '{{ ts_nodash }}' + '/',
                   '--train_steps=300'
                    ],
    labels={"job_type": "training"},
    on_success_callback=store_training_job_metrics,
    dag=dag
)


branching_task = BranchPythonOperator(
    task_id='branching',
    python_callable=check_job_relevancy,
    op_kwargs={'_ts': '{{ ts_nodash }}'},
    provide_context=True,
    dag=dag
)


version_creation_task = MLEngineVersionOperator(
    task_id='version-creation',
    project_id=os.environ['GCP_PROJECT'],
    model_name=MODEL_NAME,
    version={
        'name': 'version_{{ ts_nodash }}',
        'deployment_uri': '{{ ti.xcom_pull(task_ids="branching", key="model-location") }}',
        'runtime_version': '1.14',
        'machineType': 'mls1-c1-m2',
        'framework': 'TENSORFLOW',
        'pythonVersion': '3.5',
    },
    dag=dag
)

stop_task = BashOperator(
    task_id='stop',
    bash_command='echo The retrained model is not better than the current one!',
    dag=dag,
)


default_version_setting_task = MLEngineSetDefaultVersionOperator(
    task_id="default-version-setting",
    project_id=os.environ['GCP_PROJECT'],
    model_name=MODEL_NAME,
    version_name='version_{{ ts_nodash }}',
    dag=dag
)


# Define tasks dependencies
training_task >> branching_task >> version_creation_task >> default_version_setting_task
branching_task >> stop_task