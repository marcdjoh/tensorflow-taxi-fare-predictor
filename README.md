# tensorflow-taxi-fare-predictor
This project demonstrates how to enable automatic tensorflow machine learning model retraining using
Airflow. The retraining strategy relies on a comparison between the training dataset target mean value
and the predictions mean value.
A retraining is automatically triggered by an Airflow DAG if the difference between those two values is
high.


# Technologies
- Google Cloud Platform
- AI Platform
- Tensorflow
- Google Cloud SQL / PostgreSQL
- Google Cloud Composer / Apache Airflow



## Clone the repository
- Using https
```
git clone https://gitlab.com/marcdjoh/tensorflow-taxi-fare-predictor.git
```

- Using ssh
```
git clone git@gitlab.com:marcdjoh/tensorflow-taxi-fare-predictor.git
```

## Learn more
You can learn more about this project by reading the following [medium article]()



# Author
For any questions and/or comments, please contact **marcgeremie@gmail.com**
